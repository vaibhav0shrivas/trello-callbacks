
const path = require("path");
const cards = require(path.join(__dirname, "data/cards.json"));

/**
* getAllCardsInTheListByListID will return all cards that belong to a particular list based on the
listID that is passed to it from the given data in cards.json.
Then pass control back to the code that called it by using a callback function.
* @param {String} passedListID 
* @param {Function} callback 
* @returns 
*/
const getAllCardsInTheListByListID = function (passedListID, callback) {
    if (typeof passedListID !== 'string' || passedListID === undefined || typeof callback !== 'function') {
        if (typeof callback === 'function') {
            callback(new Error("Invalid Arguments to getAllCardsInTheListByListID()."));
        }
        console.error("Invalid Arguments to getAllCardsInTheListByListID().");
        return;
    }
    let randomSeconds = Math.floor(Math.random() * (7 - 2) + 2); // random number between 2-7
    setTimeout(() => {
        const cardsOnList = cards[passedListID];
        if (cardsOnList === undefined) {
            callback(new Error(`List ${passedListID} does not have any cards on it!`));
        }
        else {
            callback(null, cardsOnList);
        }

    }, randomSeconds * 1000);
};

module.exports = getAllCardsInTheListByListID;