const path = require("path");
const boards = require(path.join(__dirname, "data/boards.json"));
const getBoardDetailsByID = require(path.join(__dirname, "callback1.cjs"));
const getListsOnBoardByBoardID = require(path.join(__dirname, "callback2.cjs"));
const getAllCardsInTheListByListID = require(path.join(__dirname, "callback3.cjs"));

/**
* This function will use getBoardDetailsByID(),getListsOnBoardByBoardID() 
* and getAllCardsInTheListByListID() to get the following information.
* Get information from the Thanos boards
* Get all the lists for the Thanos board
* Get all cards for the Mind and space list simultaneously.
* @param {String} passedBoardName 
* @returns 
*/
const getWholeBoardByBoardNameMindAndSpace = function (passedBoardName) {
    if (typeof passedBoardName !== 'string' || passedBoardName === undefined) {
        console.error("Invalid Arguments to getWholeBoardByBoardNameMindAndSpace().");
        return;
    }
    let randomSeconds = Math.floor(Math.random() * (7 - 2) + 2); // random number between 2-7
    setTimeout(() => {
        const boardDetails = boards.filter(function (board) {
            return board.name === passedBoardName;
        });

        if (boardDetails.length == 0) {
            console.error(`Board not found with Name = ${passedBoardName}!`);
        }
        else {
            const boardId = boardDetails[0].id;
            const listNamesById = {
                "qwsa221": "Mind",
                "jwkh245": "Space"
            };
            let result = {
                BoardDetails: [],
                ListsOnBoard: [],
                CardsOnList: {
                    "Mind": [],
                    "Space": []
                }
            };
            getBoardDetailsByID(boardId, (error, resultBoardDetails) => {
                if (error) {
                    console.error(error);
                } else {
                    result.BoardDetails = result.BoardDetails.concat(resultBoardDetails);
                    console.log("BoardDetails: ", result.BoardDetails);
                    getListsOnBoardByBoardID(boardId, (error, resultListOnBoard) => {
                        if (error) {
                            console.error(error);
                        } else {
                            result.ListsOnBoard = result.ListsOnBoard.concat(resultListOnBoard);
                            console.log("ListOnBoard: ", result.ListsOnBoard);
                            let listTobeShown = result.ListsOnBoard.filter((listObject) => {
                                if (listObject.id in listNamesById) {
                                    return true;
                                }

                            }).map((listObject) => {
                                return [listObject.id, listObject.name];
                            });
                            let remainingListToGetCards = listTobeShown.length;
                            if (listTobeShown.length == 0) {
                                console.log("CardsOnList", result.CardsOnList);
                            } else {
                                listTobeShown = Object.fromEntries(listTobeShown);
                                Object.keys(listTobeShown).forEach((listId) => {
                                    getAllCardsInTheListByListID(listId, (error, resultWithAllCards) => {
                                        remainingListToGetCards--;
                                        if (error) {
                                            console.error(error);
                                        } else {
                                            result.CardsOnList[listNamesById[listId]] = result.CardsOnList[listNamesById[listId]].concat(resultWithAllCards);

                                        }
                                        if (remainingListToGetCards == 0) {
                                            console.log("CardsOnList", result.CardsOnList);
                                        }


                                    });
                                });
                            }

                        }

                    });

                }
            });


        }

    }, randomSeconds * 1000);
};

module.exports = getWholeBoardByBoardNameMindAndSpace;