
const path = require("path");
const boards = require(path.join(__dirname, "data/boards.json"));

/**
* getBoardDetailsByID willreturn a particular board's information
based on the boardID that is passed from the given list of boards in boards.json
and then pass control back to the code that called it by using a callback function.
* @param {String} passedBoardID 
* @param {Function} callback 
*/
const getBoardDetailsByID = function (passedBoardID, callback) {
    if (typeof passedBoardID !== 'string' || passedBoardID === undefined || typeof callback !== 'function') {
        if (typeof callback === 'function') {
            callback(new Error("Invalid Arguments to getBoardDetailsByID()."));
        }
        console.error("Invalid Arguments to getBoardDetailsByID().");
        return;
    }
    let randomSeconds = Math.floor(Math.random() * (7 - 2) + 2); // random number between 2-7
    setTimeout(() => {
        const boardDetails = boards.filter(function (board) {
            return board.id === passedBoardID;
        });

        if (boardDetails.length == 0) {
            callback(new Error(`Board not found with ID = ${passedBoardID}!`));
        }
        else {
            callback(null, boardDetails);
        }

    }, randomSeconds * 1000);
};

module.exports = getBoardDetailsByID;