
const testGetListsOnBoardByBoardID = require("../callback2.cjs");

testGetListsOnBoardByBoardID("");
testGetListsOnBoardByBoardID();
testGetListsOnBoardByBoardID(1);
testGetListsOnBoardByBoardID({},23);
testGetListsOnBoardByBoardID([12],23);

testGetListsOnBoardByBoardID("invalidid",(error,result) => {
    if(error){
        console.error(error);
    }else{
        console.log(result);
    }
});

console.log("------------------------------------------");

testGetListsOnBoardByBoardID("abc122dc",(error,result) => {
    if(error){
        console.error(error);
    }else{
        console.log(result);
    }
});


testGetListsOnBoardByBoardID("mcu453ed",(error,result) => {
    if(error){
        console.error(error);
    }else{
        console.log(result);
    }
});

testGetListsOnBoardByBoardID("mcu453ed","Asas");

