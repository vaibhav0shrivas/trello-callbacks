
const testGetBoardDetailsByID = require("../callback1.cjs");

testGetBoardDetailsByID("");
testGetBoardDetailsByID();
testGetBoardDetailsByID(1);
testGetBoardDetailsByID({},23);
testGetBoardDetailsByID([12],23);

testGetBoardDetailsByID("invalidid",(error,result) => {
    if(error){
        console.error(error);
    }else{
        console.log(result);
    }
});

console.log("------------------------------------------");

testGetBoardDetailsByID("abc122dc",(error,result) => {
    if(error){
        console.error(error);
    }else{
        console.log(result);
    }
});


testGetBoardDetailsByID("mcu453ed",(error,result) => {
    if(error){
        console.error(error);
    }else{
        console.log(result);
    }
});

testGetBoardDetailsByID("mcu453ed","Asas");

