const path = require("path");
const boards = require(path.join(__dirname, "data/boards.json"));
const lists = require(path.join(__dirname, "data/lists.json"));

/**
 * getListsOnBoardByBoardID will return all lists that belong to a board based on the boardID 
that is passed to it from the given data in lists.json.
Then pass control back to the code that called it by using a callback function.
 * @param {String} passedBoardID 
 * @param {Function} callback 
 */
const getListsOnBoardByBoardID = function (passedBoardID, callback) {
    if (typeof passedBoardID !== 'string' || passedBoardID === undefined || typeof callback !== 'function') {
        if (typeof callback === 'function') {
            callback(new Error("Invalid Arguments to getListsOnBoardByBoardID()."));
        }
        console.error("Invalid Arguments to getListsOnBoardByBoardID().");
        return;
    }
    let randomSeconds = Math.floor(Math.random() * (7 - 2) + 2); // random number between 2-7
    setTimeout(() => {
        const boardDetails = boards.filter(function (board) {
            return board.id === passedBoardID;
        });

        if (boardDetails.length == 0) {
            callback(new Error(`Board not found with ID = ${passedBoardID}!`));
        }
        else {
            let listOnBoard = lists[passedBoardID];
            if (listOnBoard === undefined) {
                callback(new Error(`Board ${passedBoardID} does not has any lsit on it!`));
            }
            else {
                callback(null, listOnBoard);
            }
        }

    }, randomSeconds * 1000);
};

module.exports = getListsOnBoardByBoardID;