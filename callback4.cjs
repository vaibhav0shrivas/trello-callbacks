
const path = require("path");
const boards = require(path.join(__dirname, "data/boards.json"));
const getBoardDetailsByID = require(path.join(__dirname, "callback1.cjs"));
const getListsOnBoardByBoardID = require(path.join(__dirname, "callback2.cjs"));
const getAllCardsInTheListByListID = require(path.join(__dirname, "callback3.cjs"));

/**
* This function will use getBoardDetailsByID(),getListsOnBoardByBoardID() 
* and getAllCardsInTheListByListID() to get the following information.
* Get information from the Thanos boards
* Get all the lists for the Thanos board
* Get all cards for the Mind list simultaneously.
* @param {String} passedBoardName 
* @returns 
*/
const getWholeBoardByBoardName = function (passedBoardName) {
    if (typeof passedBoardName !== 'string' || passedBoardName === undefined) {
        console.error("Invalid Arguments to getWholeBoardByBoardName().");
        return;
    }
    let randomSeconds = Math.floor(Math.random() * (7 - 2) + 2); // random number between 2-7
    setTimeout(() => {
        const boardDetails = boards.filter(function (board) {
            return board.name === passedBoardName;
        });

        if (boardDetails.length == 0) {
            console.error(`Board not found with Name = ${passedBoardName}!`);
        }
        else {
            const boardId = boardDetails[0].id;
            const listId = "qwsa221";
            let result = {
                BoardDetails: [],
                ListsOnBoard: [],
                CardsOnList: { "Mind": [] }
            }
            getBoardDetailsByID(boardId, (error, resultBoardDetails) => {
                if (error) {
                    console.error(error);
                } else {
                    result.BoardDetails = result.BoardDetails.concat(resultBoardDetails);
                    getListsOnBoardByBoardID(boardId, (error, resultListOnBoard) => {
                        if (error) {
                            console.error(error);
                        } else {
                            result.ListsOnBoard = result.ListsOnBoard.concat(resultListOnBoard);
                            getAllCardsInTheListByListID(listId, (error, resultAllCardsOnList) => {
                                if (error) {
                                    console.error(error);
                                } else {
                                    result.CardsOnList.Mind = result.CardsOnList.Mind.concat(resultAllCardsOnList);
                                    console.log(result);
                                }
                            });
                        }

                    });

                }
            });


        }

    }, randomSeconds * 1000);
};

module.exports = getWholeBoardByBoardName;